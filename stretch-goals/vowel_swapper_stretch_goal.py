def vowel_swapper(string):
    # ==============
    # Your code here
    swapList = {"a": "4", "A": "4", "e": "3", "E": "3", "i": "!", "I": "!", "o": "ooo", "O": "000", "u": "|_|", "U": "|_|"}
    occurences = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0}
    string = list(string)
    for letterID in range(len(string)):
        if string[letterID] in swapList.keys():
            occurences[string[letterID].lower()] += 1
            if occurences[string[letterID].lower()] == 2:
                string[letterID] = swapList[string[letterID]]
    
    return "".join(string)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
